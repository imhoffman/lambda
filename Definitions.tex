\renewcommand*{\marginfont}{\color{blue}\sffamily}
\section{Definitions}

\subsection{Everyday Mathematical Notation}

The discussion begins with examples of what Smith\footnote{Smith, Peter.\ An Introduction to Godel’s Theorems, 2nd ed.\ Cambridge: Cambridge University Press, 2013.} calls ``everyday'' mathematics; that is, the ideas and notation that are familiar from secondary school.
The exposition here shall follow Church's in that familiar everyday statements shall be re-expressed using $\lambda$ notation.
Then, an additional step shall be taken: re-expressing the statements in Scheme and CL.

%NB: However, don't expect to see functions like sine nor irrational numbers like $\pi$ in our examples.
%Very little of the literature on $\L$ investigations---and none of this document---addresses complicated composite functions such as sine, nor real numerical arguments other than the positive integers.

\noindent{\bf Example \#1}\\
Consider the everyday statement $f(x)$, ``$f$ of $x$.''
In contemporary $\L$ notation, this idea is expressed as simply
\[
fx
\]
without any parentheses or $\L$'s.

The meaning is that there is a function $f$ that takes the argument $x$ and yields/returns $fx$.
Note that $f$ is not defined by this expression; $f$ would be defined elsewhere.

In programming languages, it is generally allowed that names for variables and functions are not limited to one character.\footnote{Church envisions, in principle, an ``enumerably infinite'' character set, reminiscent of Turing's (1936) discussion of the many different single characters that could be made with ink on a square of paper\ldots\ We'll simply stick with ASCII and understand {\tt f1} and {\tt f2} to be different functions!}
That is, if {\tt fx} or {\tt f1} or {\tt func} are entered into a computer program, it is best if {\tt f1} is understood as a single symbol, not as ``$f$ of $1$.''
%For that reason, both Scheme and CL allow strings of characters (like ``lambda'') to be single symbols in their own right.
After only one page of Church's paper, it is found that $\L$-based programming languages justifiably depart from Church's $\L$ syntax.
In Lisp, ``$f$ of $x$'' is communicated as follows.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{racket}
(f x)
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{lisp}
(f x)
\end{minted}
\end{minipage}

\noindent
That is, the {\tt f} and {\tt x} need to be delimited by white space, yet they also need to be associated with each other.
The association of {\tt x} as an argument to {\tt f} is achieved by surrounding them in parentheses.

Church also leads off his Definitions with an example of ``$f$ of $x$'' on p.~347.
\begin{figure}[h]
  \centering
      \includegraphics[width=0.50\textwidth, angle=000]{images/church-x.png}
%      \caption{p. 347\label{x}}
\end{figure}
Church uses $x$ as his dummy independent variable.
He notes that it is a ``well-formed formula,'' meaning simply that such notation is allowed in the $\L$ system.
However, his ``$f$ of $x$'' example does {\em not} use the simple $fx$ notation.
\begin{figure}[h]
  \centering
      \includegraphics[width=0.75\textwidth, angle=000]{images/church-FX.png}
%      \caption{p. 347\label{FX}}
\end{figure}
Rather, in his example of ``$f$ of $x$'' he uses parentheses in the everyday sense, he also uses curly braces, and he uses capital letters despite having already introduced lower-case $\bm{x}$ as a ``variable.''
It is instructive to unpack Church's notation conceptually, especially since neither Lisp nor contemporary mathematical texts employ parentheses or braces in Church's original manner.

Church's capital letter $\bm{X}$ signifies that the argument of $\bm{F}$ need not be, in general, a single, stand-alone value or variable---it shall be shown that $\bm{X}$ may represent a lengthy expression or even a function.
In general, $\bm{F}$ may take a complicated function as its argument, meaning that ``$F$ of $X$'' is itself a function.
So, the argument in Church's example is given as the general $\bm{X}$ rather than the less-general $\bm{x}$.
Church is envisioning separate namespaces for variables and functions---something that exists in CL but not in Scheme; a topic that will be revisited.

Unlike $\bm{X}$, $\bm{F}$ {\em must} be a function because it is accepting arguments, and so Church naturally represents it with a capital letter.
The fact that $\bm{F}$ is certainly a function is further communicated by Church by enclosing it in curly braces.
However, the additional notation of curly braces is unnecessary; indeed, Church eventually drops it and it shall not be seen in either Scheme or CL.
Although Church's notation is not ultimately {\em typed}\marginnote{typed}, most programming languages are---it is useful to be able to distinguish symbols that represent function instructions from symbols that are merely variable values.

Church reiterates his function-and-application notation on p.~357.
\begin{figure}[h]
  \centering
      \includegraphics[width=0.75\textwidth, angle=000]{images/church-curly.png}
%      \caption{p. 347\label{x}}
\end{figure}
It has already been noted that programming languages are not limited to single-character names; sometimes functions are named {\tt f1} or {\tt func} rather than simply {\tt f}.
Thus, in this Tutorial is adopted a ``hat'' notation for both one- and multi-character names instead of enclosing the names in curly braces like Church.
When otherwise ambiguous or important, ``$f$ of $x$'' shall be expressed as
\[
\what{f}x
\]
and ``{\tt f1} of {\tt x}'' as $\what{f1}\,x$ and ``{\tt func} of {\tt x}'' as $\what{func}\,x$.
%As programmers, it is generally needed to conceptually distinguish between the symbol for a function $\what{f}$ and the symbol for the value $x$ that is the argument of the function.

Not only are the curly braces not needed in Church's expression $\{\bm{F}\}(\bm{X})$, neither are the parentheses.
The $\bm{X}$ following the $\bm{F}$ already means that $\bm{X}$ is the argument to $\bm{F}$.
%The parentheses that are familiar from everyday notation are not needed.
Since parentheses are used for grouping in Lisp, using parentheses as Church does---surrounding a single symbol that does not represent a return---is confusing in contemporary notation.
So, in this Tutorial such parentheses shall be omitted, leaving Church's expression $\{\bm{F}\}(\bm{X})$ to be equivalently written as $\bm{F}\bm{X}$, $\what{F}X$, or simply $FX$.

\noindent{\bf Example \#2}\\
Consider now the simple example in which the argument $\bm{X}$ is merely a variable $\bm{x}$ that has the value 17.
It shall be assumed that the functional form of $f$ is known and that the return value of the function when the argument is 17 can be computed.

The notation for operating $f$ on 17 is no different than for $x$.
In $\L$ notation, one symbol up against another means that the symbol on the left operates on the symbol on the right.
So, just as
\[
fx\qquad\text{or}\qquad\what{f}\,x
\]
means ``$f$ of $x$,'' so too does
\renewcommand*{\marginfont}{\color{red}\sffamily}
\[
f17\marginnote{confusing}
\]
mean ``$f$ of 17.''
Of course, this notation is unclear because humans usually use two symbols to represent the value ``seventeen,'' but the $\L$ calculus is a symbolic analysis tool not intended for hauling literals around.
Putting parentheses around ``17'' does not alleviate the problem.
Indeed, Church and Turing would not write ``17'' at all, because one cannot help but convey ``1 acting on 7'' no matter how one uses the parentheses.
Instead, Church or Turing would do an ink-on-paper study of ``seventeen'' and come up with some single symbol---say, $\mho$---and then could write
\[
%\left(\L{x}.fx\right)\mho = f\mho\ .
f\,\mho\ .
\]

Again, a contemporary computer programmer is free to use multi-character symbols and Lisp has been built accordingly.
Employing delimiting white space as previously, the following codes are obtained.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{racket}
(f 17)
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{lisp}
(f 17)
\end{minted}
\end{minipage}

\noindent
In both languages {\tt f} is not pressed up against ``17'' any more than it was pressed up against {\tt x} in the preceding code example.
%In both cases, blank space appears between $f$ and its argument.
%The outer parentheses that surround $f$ and its argument are necessary for both Scheme and CL---even though they are optional for Church.

Later, it shall be described that numerals in $\L$ calculus are functions, not literals.
With this recognition, the ambiguity of ``17'' can be obviated with hat notation: $\what{17}$ means numeral ``seventeen'' while $\what{1}\what{7}$ means ``function 1 acting on function 7.''

\noindent{\bf Example \#3}\\
It is rare that literals shall be passed as arguments.
It is far more important to be able to make a symbolic statement such as $f\left(g(x)\right)$---that is, ``$f$ of $g$ of $x$''---in which the return from $g$ is an argument to $f$.
In $\L$ notation, this is simply
\renewcommand*{\marginfont}{\color{green}\sffamily}
\[
fgx\ .\marginnote{OK}
\]
The right-to-left priority is essential; the steps occur in this order: (1) the value $x$ is fed to the function $g$, (2) the function $g$ returns a value that depends on $x$, (3) the value returned from $g$ gets fed to the function $f$, and finally (4) the function $f$ returns a value that depends on the value returned from $g$.
In order to remember this order, one may correctly write
\[
f(gx)\marginnote{OK}
\]
but adding the following additional parentheses
\renewcommand*{\marginfont}{\color{red}\sffamily}
\[
f\left(g\textcolor{red}{(}x\textcolor{red}{)}\right)\marginnote{confusing}
\]
\renewcommand*{\marginfont}{\color{green}\sffamily}
would be mixing the meaning of the parentheses between their $\L$ meaning and their ``everyday'' meaning.
If the desire is to stress functions and arguments, the hat notation may be employed:
\[
\what{f}(\what{g}\,x)\qquad\text{or}\qquad(\what{f}(\what{g}\,x))\qquad\text{or}\qquad\what{f}\,\what{g}\,x\ .\marginnote{OK}
\]

As before, Lisp expects a proper, consistent use of parentheses that looks a lot like
\[
(f(gx))\ .\marginnote{OK}
\]

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{racket}
(f (g x))
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{lisp}
(f (g x))
\end{minted}
\end{minipage}



\subsection{Free and Bound symbols}
\renewcommand*{\marginfont}{\color{blue}\sffamily}
In the preceding example of ``$f$ of $x$,'' it is {\em not} assumed that $x$ is meaningful to $f$---that is, if $f$ were written out as instructions, there need not be $x$'s in $f$'s formula.
In the preceding text, the variable $x$ is a utility for {\em us} to imagine operating $f$ on different values; $x$ is not a utility for $f$.
A stronger statement that has not yet been made is that ``$f$ is a function of $x$''---that is, $x$ appears in the Church formula of $f$.
The running guide for this section will be to unpack and formalize the statement ``$f$ is a function of $x$.''


\subsubsection{Binding}

The internal formulation of $f$ shall eventually be stated in this section.
For now, it is presupposed that ``$f$ is a function\ldots'' because the discussion centers on $\what{f}$ taking arguments and returning a value.

In the expression $\what{f}x$, $x$ is a {\em free}\marginnote{free} variable because it does not possess an explicit identity with regard to $\what{f}$.
That is, $x$ is the value of the argument that is going to be fed to $\what{f}$.
(In computing, $x$ is not known until runtime.)
For passing a value to $f$, the expressions $\what{f}w$ or $\what{f}v$ would work just as well.
The irrelevance of a user's arbitrary (free) choices are apparent from Church's rules on p.~347.

\begin{figure}[h]
  \centering
      \includegraphics[width=0.75\textwidth, angle=000]{images/church-I-II-III.png}
%      \caption{p. 347\label{rules}}
\end{figure}

It is operation~III that shall be exploited here.
In other words, Church qualifies operation~III by stating ``{\it not immediately following $\L$}'' because that location in the syntax is special.
In the statement ``$f$ is a function of $x$,'' a $\L$ is the tool for making the expression ``\ldots\ of $x$'' rather than ``of'' something else.
That is, $\what{f}$ explicitly uses the symbol $x$ (not $v$ or $w$) within its (as yet unseen) internal formulation; $x$ is {\em bound}\marginnote{bound} by the $\L$.

The road to expressing fully that ``$f$ is a function of $x$'' begins with understanding how Church uses $\bm{M}$ in his example of $\L$ binding.

\begin{figure}[h]
  \centering
      \includegraphics[width=0.75\textwidth, angle=000]{images/church-bound.png}
%      \caption{p. 346\label{FX}}
\end{figure}

\noindent Church's description amounts to
\[
\L{x}.Mx
\]
which does {\em not} mean ``$M$ is a function of $x$.''
Rather, $M$ are the instructions that are performed on $x$ in order to return $Mx$.
For example, if our function were
\[
\sin(3x+\pi)
\]
then $M$ would comprise the sine, the 3, the $\pi$, the $+$, and perhaps some notion of multiplication that is portable to a non-integer value of $x$, and perhaps also whether ``sine'' is a look-up table or a Taylor expansion\ldots\ etc.
The idea is that $M$ would comprise many things; but $M$ would {\em not} be the name of the function.
Rather, $M$ would be elsewhere defined as a string of symbols constituting a ``well-formed formula'' that represents the sine of pi plus three of the argument.
%NB: Our $\lambda$ notation in this introductory section stops short of a complete definition of the operations in the example functions.
%The development of a primitive notion/notation of, say, ``multiplication'' is the end goal of the $\lambda$ calculus (and of this document).
%In this section, we describe all of the $\L$ semantics, but not all of the $\L$ syntax.

In fact, the function that returns $Mx$ does not have a name.\marginnote{anonymous}
The whole point of a $\L$ expression is to bind up values and functionality into a single return that does not have a callable name nor any persistence.
Because every return is simply fed as an argument into another waiting function without being named or otherwise held as a variable, $\L$ expressions are described as {\em anonymous}.

Having a free variable means that information must be added in order for the expression to be evaluated.
Equivalently, if all of the variables are bound, then the expression is ready to run.
Anonymity manifests in Lisp as a recognition that instructions exist but that they do not have a name; without values on which to operate, the interpreter system can only return that the expressions are value-less instructions---a {\tt procedure} or a {\tt function}.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[escapeinside=||]{racket}
> (lambda (x) (* x 3))|\label{def:lbound}|
#<procedure>
> ((lambda (x) (* x 3)) 7)|\label{def:eval}|
21
> (lambda (u) (* u v))|\label{def:lfree}|
#<procedure>
> ((lambda (u) (* u v)) 7)
v: undefined;
> ((lambda (u) (* u v)) 7 3)
#<procedure>: arity mismatch;
 the expected number of arguments does not match the given number
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}{lisp}
[1]> (lambda (x) (* x 3))
#<FUNCTION :LAMBDA (X) (* X 3)>
[2]> ((lambda (x) (* x 3)) 7)
21
[3]> (lambda (u) (* u v))
#<FUNCTION :LAMBDA (U) (* U V)>
[4]> ((lambda (u) (* u v)) 7)
*** - :LAMBDA: variable V has no value
[5]> ((lambda (u) (* u v)) 7 3)
*** - EVAL/APPLY: too many arguments given to :LAMBDA
\end{minted}
\end{minipage}

\noindent
In the pursuit of the statement ``$f$ is a function of $x$,'' here the Lisp interpreter is recognizing the ``\ldots\ is a function\ldots'' part.
When the user chooses $x$ or $u$ or $v$ appropriately, that function becomes ``\ldots\ of $x$'' or ``\ldots\ of $u$'' or ``\ldots\ of $v$.

\begin{wrapfigure}[12]{r}{0.39\textwidth}
\vspace{-0.15in}
\colorbox{lightgray}{%
\begin{minipage}{\dimexpr0.38\textwidth-2\fboxrule-2\fboxsep\relax}
Obviously, printed expositions of Church's work makes use of the symbol ``$\lambda$.''
In some computing environments, it is possible to use the Greek character within code to the effect that is seen here.
Nevertheless, the ASCII string {\tt lambda} will always work in Lisp (and in Python, etc.) and the machine code in this Tutorial is limited to ASCII.
\end{minipage}}
\end{wrapfigure}
At the interpreter prompt, a $\L$ expression may be entered and recognized as yet-unapplied function instructions, whether the variables are bound or not.
The expression on line~\ref{def:lbound} has $x$ which is bound.
The expression on line~\ref{def:lfree} has $u$ which is bound and $v$ which is not bound.
In Church's words, the $v$ is ``{\it not immediately following $\L$}'' and so is still free.

In order to evaluate an expression, the instructions in the {\tt lambda} must be applied to a value.
The instructions on line~\ref{def:lbound} are successfully applied to the value {\tt 7} on line~\ref{def:eval}.
However, the expression on line~\ref{def:lfree} is unable to be executed even though it is syntactically correct.
Since $v$ is not bound in the second expression, there is no way to pass a value for $v$ into the function instructions.

Beware that Church breaks his own rule of ``{\it immediately following $\L$}'' before the end of the paper.
Frankly, most authors break this rule, too---and so does Lisp.
In Church's words on p.~347:

\begin{figure}[h]
  \centering
      \includegraphics[width=0.75\textwidth, angle=000]{images/church-abbrev.png}
%      \caption{p. 347\label{FX}}
\end{figure}

\noindent Indeed, the first code formulation below is consistent with Church's proposed abbreviation.
The second code formulation is equivalent to the first but employs binding only one ``immediate'' variable at a time.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{racket}
((lambda (u v)
  (- u v)) 7 4)
;Value: 3

((lambda (v)
  ((lambda (u)
    (- u v)) 7)) 4)
;Value: 3
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{lisp}
> ((lambda (u v)
    (- u v)) 7 4)
3

> ((lambda (v)
    ((lambda (u)
      (- u v)) 7)) 4)
3
\end{minted}
\end{minipage}

\noindent
The abbreviated notation can be quite confusing, both in code and in written work.
For example, the {\tt (u v)} following the {\tt lambda} is not a function application; it is merely a grouping.
Indeed, since only one symbol can ``immediately'' follow the $\L$, the rule must be amended.
In Lisp code there is {\tt ()} grouping and in Church formulation there is enclosure between the ``.''\ and the $\L$.
The Lisp expressions above are written out as
\[
\L{uv}.Muv \quad\equiv\quad \L{v}.(\L{u}.Muv)v\ .
\]


\subsubsection{$\eta$ Expansion}

The act of formally binding certain symbols to pre-existing instructions is explicitly addressed by Church.
%Three Rules Here ?
Consider the difference between the following:
\[
M\quad\text{and}\quad\L{x}.Mx\ .
\]
The instructions $M$ (which is itself some string of symbols) is no different in either case.
The utility of the $\L$ is that the passing in of an argument and the return from the expression can be prescribed and followed.
This relationship is $\eta$-equivalence; that is, $\L{x}.Mx$ is the $\eta$-expansion of $M$.

Consider also the identity operation on the single symbol $\bm{x}$ which, according to Church, is a ``well-formed formula'' that represents ``one positive integer''
\[
x\quad\text{and}\quad\L{x}.x\quad\text{and}\quad\L{y}.\left(\L{x}.x\right)y
\]
which manifests in Lisp code as

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[escapeinside=||]{racket}
7|\label{id:literal}|
;Value: 7
((lambda (x) x) 7)|\label{id:1L}|
;Value: 7
((lambda (y)
  (lambda (x) x)
   y) 7)|\label{id:2L}|
;Value: 7
((lambda (x) x)
  ((lambda (x) x) 7))|\label{id:LL}|
;Value: 7
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}{lisp}
> 7
7
> ((lambda (x) x) 7)
7
> ((lambda (y)
   (lambda (x) x)
    y) 7)
7
> ((lambda (x) x)
   ((lambda (x) x) 7))
7
\end{minted}
\end{minipage}

It is important to note that while Church's $\bm{x}$ is a well-formed formula when representing ``one positive integer,'' it is {\em not} a procedure with a return.
However, in Lisp, {\tt 7} returns ``7.''
It is a peculiarity of functional programming languages that everything returns something for the sake of being read, evaluated, and printed at the prompt.
Church---more so than Lisp---distinguishes between the natural numbers and procedures that return the natural numbers.
(NB: The literal {\tt 7} and the return ``7'' should not be confused for Church's distinction between $\bm{m}$ and $m$ on p.~349.)

The first three $\L$ expressions in the Lisp code are equivalent in the sense that they will always all return the value of their argument.
For the sake of mathematical proofs, this equivalence is worth delivering as a postulate with a spiffy name; for programming and machine code, it is a utility for manipulating the placement of the instructions and arguments without altering the return.


\subsubsection{Assignment}

With $x$ bound via $\eta$-expansion, $f$ as a function of $x$ is represented as the {\em instructions} $M$ of the function $f$ such that the name $f$ does not appear at all anymore:
\[
\L{x}.Mx
\]
in $\L$ notation.
The character following the $\L$ is the independent variable whose value can be passed in as an argument.
Although it is not uncommon to encounter the following notation---including in Church's original paper---
\renewcommand*{\marginfont}{\color{red}\sffamily}
\[
\L{x}.M\textcolor{red}{(}x\textcolor{red}{)}\marginnote{confusing}
\]
for function application, it is inappropriate to use parentheses in this manner in Lisp.
In this document, parentheses are reserved for grouping, as shall be seen.

The power of $\L$ notation is that simply $Mx$ represents the application of the instructions $M$ to $x$.
Read left-to-right in this way, $M$ is obviously an instruction that is operating on $x$---such ordering will {\em always} unambiguously communicate this meaning.
Nevertheless, if it must be emphasized that $M$ are instructions (that are taking $x$ as an argument), $M$ shall be denoted with a hat $\what{M}$ as in
\[
\L{x}.\what{M}x\ .
\]
An appropriate use of parentheses is
\[
\L{x}.(Mx)\ ,
\]
which is equivalent to $\L{x}.Mx$.
In Church notation, the parentheses in the preceding case are optional because $Mx$ communicates all that needs to be known.
However, in a programming language, $Mx$ would be seen as a single symbol/name, not as $M$ and $x$ in an operator--argument relationship.

Using parentheses to surround the application of $f$ to $x$ with white space between the $f$ and $x$ is how $\L$-based Lisp languages actualize the idea.

The only special characters in Church's notation are $\L$, {\tt .}, {\tt (}, and {\tt )}.
So, all of the following have the same meaning:
\[
\L{x}.Mx\qquad\text{and}\qquad\L{v}.Mv\qquad\text{and}\qquad\L{t}.(Mt)\qquad\text{and}\qquad\L{x}.\left(\what{M}x\right)
\]
because the $\L$ and the .\ are what matters to the $M$.

\renewcommand*{\marginfont}{\color{blue}\sffamily}
Associating the name\marginnote{named} $f$ with the instructions $M$ is represented in the mathematical and computing literature variously as
\[
f \leftarrow Mx,\quad f = Mx, \quad f \equiv Mx\ .
\]
Among the various ideas that are being communicated are ``$f$ is the same as $Mx$'' or ``the instructions $Mx$ live at memory address $f$'' or ``$f$ maps to $Mx$'' or ``$Mx$ is assigned to $f$.''
Church describes the situation on p.~347:

\begin{figure}[h]
  \centering
      \includegraphics[width=0.75\textwidth, angle=000]{images/church-stands.png}
%      \caption{p. 347\label{stands}}
\end{figure}

\noindent
For our present need the particular sequence of symbols $\bm{A}$ would be $\L{x}.Mx$ such that
\[
f \rightarrow \L{x}.Mx
\]
is how Church would assign the name.
In Lisp, the syntax for assignment is important.
(NB: This is a very contentious point in the Lisp world.\footnote{For example, from Section~2.6 of \href{https://www.scheme.com/tspl4/start.html\#./start:h6}{The Scheme Programming Language:} `This syntax is often referred to, somewhat pejoratively, as the ``defun'' syntax for {\tt define}, after the {\tt defun} form provided by Lisp languages in which procedures are more closely tied to their names.'}
The reader is directed to the Appendix on the Y~Combinator to see this syntax in heavier action.)

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{racket}
(define f
 (lambda (x) (M x)))
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{lisp}
(defun f () (function
 (lambda (x) (M x))))
\end{minted}
\end{minipage}

\noindent
In both Lisp dialects, the goal is to achieve the ``stands for'' notion for which ``$\rightarrow$'' is used by Church.
In Scheme, the fact that {\tt f} will be representing a {\tt \#<procedure>} is not important at the syntax level; {\tt f} is defined like any other symbol.
Not so in Common Lisp!
In CL, there are two additional explicit steps:
\begin{enumerate}
\item {\tt f} is explicitly defined as a function using {\tt defun}
\item the return of the function {\tt f} is a {\tt \#<function>} and not a value; this fact is communicated using {\tt function}.
\end{enumerate}
The empty parentheses after {\tt f} in the CL {\tt defun} indicates that {\tt f} does not have any arguments; that is, it stands for $\L{x}.Mx$ no matter what {\tt x} is.

{\em Furthermore}, when the symbol {\tt f} is subsequently used, its character as a function is important:

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}{racket}
(f 7)
; returns the same as
((lambda (x) (M x)) 7)
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}{lisp}
(funcall (f) 7)
; returns the same as
((lambda (x) (M x)) 7)
\end{minted}
\end{minipage}

\noindent
In both cases, the {\tt 7} is an arbitrary argument.

It is important to note both similarities and differences here.
An important similarity is that the syntax in line~3 is no different in the different dialects.
That is, getting the system to execute $\L{x}.Mx$ when $x$ is $7$ is the same in the two dialects.
However, defining and using $f$ when it ``stands for'' $\L{x}.Mx$ is quite different.

In all, the CL ``stands for'' requires
\begin{enumerate}
\item {\tt f} to be {\tt defun}'ed as a function
\item the return of {\tt f} to be typed as a {\tt function}
\item {\tt f} is operated upon no arguments using {\tt (f)}
\item the return from {\tt (f)} is a function that is explicitly caught and used as an operation using {\tt funcall}.
\end{enumerate}
These same steps are obviously occurring within Scheme somewhere, but not in a way that requires syntactical input by the proximate user.
Thus, compared with Scheme, CL is much closer to Church's namespace delineation of capital-letter symbols and curly braces.
Indeed, mathematicians like Church are generally interested in distinguishing functions from values.
However, since it is not strictly necessary to make such a distinction at the end-user level, Scheme has managed to program around it.



... still TO DO

In this section, the Lisp code has illustrated two important points:
\begin{itemize}
\item[-] Revisit Church's \{ \} namespace... with a PDF excerpt?
\item[-] revisit why too many parentheses (as in $\L{x}.f(x)$) is inappropriate in CL and not even allowed in Scheme...
\end{itemize}

Use the Theorems on p.~348 and show that we are eta-expanding for utility, even though it makes the formula increasingly less ``normal.''
\begin{figure}[h]
  \centering
      \includegraphics[width=0.75\textwidth, angle=000]{images/church-theorems-I-II-III.png}
%      \caption{p. 348\label{theorems}}
\end{figure}

Revisit the Identity behaviour from before, but define $C\rightarrow\L{x}.x$ and $D\rightarrow\L{y}.y$ or whatever and then call them on each other in the code...
That is, compose named functions...

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[escapeinside=||]{racket}
7|\label{id:literal}|
;Value: 7
((lambda (x) x) 7)
;Value: 7
((lambda (y)
  (lambda (x) x)
   y) 7)
;Value: 7
((lambda (x) x)
  ((lambda (x) x) 7))
;Value: 7
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}{lisp}
> 7
7
> ((lambda (x) x) 7)
7
> ((lambda (y)
   (lambda (x) x)
    y) 7)
7
> ((lambda (x) x)
   ((lambda (x) x) 7))
7
\end{minted}
\end{minipage}


\renewcommand*{\marginfont}{\color{blue}\sffamily}
In contrast to ``$f$ of $g$ of $x$,'' consider the following: ``{\em first} apply the function $f$ to the function $g$ in order to make some new function {\em and then} feed the value $x$ to that new function.''
This is an important idea.
We can already tell that its $\L$ notation is
\[
\L{x}.(fg)x\ .
\]

...
