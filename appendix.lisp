;; https://lispcookbook.github.io/cl-cookbook/functions.html
;; https://stackoverflow.com/questions/47602677/recursive-factorial-function-in-common-lisp
;; http://www.cs.sfu.ca/CourseCentral/310/pwfong/Lisp/2/tutorial2.html

;;  a proper tail-called accumulated version, for reference
(defun factorial (n &optional (a 1))
  (if (< n 2) a (factorial (1- n) (* a n))))

(format t "~&    tail-called and accumulated: ~d" (factorial 5))

;;  the "C version"
(defun factorial (n)
  (if (= n 1)              
      1                           
      (* n (factorial (- n 1))))) 

(format t "~&                the 'C' version: ~d" (factorial 5))

;;  the "fortran version"
(defun factorial () (function
  (lambda (n)
    (if (= n 0)
        1
        (* n (funcall (factorial) (- n 1))))
   ))
)

(format t "~&          The 'fortran' version: ~d" (funcall (factorial) 5))

;;  continued eta manipulations
(defun bzbz () (function
 (lambda (dummy)
  (lambda (m)
;   (format t "~& inside with m = ~d~%" m)
   (if (= m 0)
    1
    (* m (funcall (funcall dummy dummy) (- m 1)))
 )))))

(defun factorial () (function
 (lambda (n)
;  (format t "~& inside with n = ~d~%" n)
  (funcall (funcall (bzbz) (bzbz)) n)
 )))

(format t "~&           with bzbz swapped in: ~d" (funcall (factorial) 5))


;;
(defun factorial ()
  (funcall (bzbz) (bzbz)))

(format t "~&       with bzbz eta-contracted: ~d" (funcall (factorial) 5))


;;  swap out bzbz
(defun factorial ()
 ((lambda (dummy1)
   (lambda (m1)
    (if (= m1 0)
     1
     (* m1 (funcall (funcall dummy1 dummy1) (- m1 1))))))
  (lambda (dummy2)
   (lambda (m2)
    (if (= m2 0)
     1
     (* m2 (funcall (funcall dummy2 dummy2) (- m2 1))))))))

(format t "~&          with bzbz swapped out: ~d" (funcall (factorial) 5))

;;  direct application
(format t "~&   the first direct application: ~d"
 (funcall
 ((lambda (dummy1)
   (lambda (m1)
    (if (= m1 0)
     1
     (* m1 (funcall (funcall dummy1 dummy1) (- m1 1))))))
  (lambda (dummy2)
   (lambda (m2)
    (if (= m2 0)
     1
     (* m2 (funcall (funcall dummy2 dummy2) (- m2 1))))))
 ) 5)
)

;;  with x and y eta expansion
;(setf n 0)
;(format t "~&     with x and y eta-expansion: ~d"
; (funcall
; ((lambda (dummy)
;   (lambda (x)
;    (lambda (m)
;     (if (= m 0)
;      1
;      (* m (funcall (x) (- m 1))))))
;       (lambda (y) (format t "~& in the 1st y~%") (funcall (funcall dummy dummy) y)))
;  (lambda (dummy)
;   (format t "~& Yaaaassss!~%")
;   (lambda (x)
;    (format t "~& Yaass!~%")
;    (lambda (m)
;     (format t "~& Yas.~%")
;     (if (= m 0)
;      1
;      (* m (funcall (x) (- m 1))))))
;       (lambda (y) (setf n (+ n 1)) (format t "~& in the 2nd y ~d times~%" n) (funcall (funcall dummy dummy) y)))
; ) 5)
;)

;; this works
;(format t "~&     with x and y eta-expansion: ~d"
;(funcall (
; (lambda (dummy)
;  (lambda (x)
;   (lambda (m)
;    (if (= m 0)
;     1
;     (* m (funcall (x) (- m 1))))))
;      (lambda (y) (funcall (funcall dummy dummy) y)))
; (lambda (dummy)
;  (lambda (m)
;   (if (= m 0)
;    1
;    (* m (funcall (funcall dummy dummy) (- m 1))))))
; ) 5)
;)
;Value: 120

(format t "~&     with x and y eta-expansion: ~d"
(funcall (
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
    1
    (* m (funcall (funcall dummy dummy) (- m 1))))))
 (lambda (dummy)
  (lambda (x)
   (lambda (m)
    (if (= m 0)
     1
     (* m (funcall (x) (- m 1))))))
      ;  this is not lazy
      (funcall (funcall dummy dummy) ))
      ;(function (funcall (funcall dummy dummy) )))
      ;(lambda (y) (funcall (funcall dummy dummy) y)))
 ) 5)
)
;Value: 120

