;;  
(define (factorial n)
  (if (= n 0)
       1
       (* n (factorial (- n 1)))))

(display "\n")
(display (factorial 5))


;;
(define factorial 
 (lambda (n)
  (if (= n 0)
   1
   (* n (factorial (- n 1))))))

(display "\n")
(display (factorial 5))


;;
(define bzbz
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
       1
       (* m ((dummy dummy) (- m 1)))
 ))))

(define factorial
  (lambda (n)
    ((bzbz bzbz) n)
    ))

(display "\n")
(display (factorial 5))


;;
(define factorial
  (bzbz bzbz))

(display "\n")
(display (factorial 5))


;;
(define factorial (
 (lambda (dummy1)
  (lambda (m1)
   (if (= m1 0)
    1
    (* m1 ((dummy1 dummy1) (- m1 1))))))
 (lambda (dummy2)
  (lambda (m2)
   (if (= m2 0)
    1
    (* m2 ((dummy2 dummy2) (- m2 1))))))))

(display "\n")
(display (factorial 5))


;;
(display "\n")
(display
((
 (lambda (dummy1)
  (lambda (m1)
   (if (= m1 0)
    1
    (* m1 ((dummy1 dummy1) (- m1 1))
 ))))
 (lambda (dummy2)
  (lambda (m2)
   (if (= m2 0)
    1
    (* m2 ((dummy2 dummy2) (- m2 1))
 ))))) 5)
)


;;
(display "\n")
(display
((
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
    1
    (* m ((dummy dummy) (- m 1))
 ))))
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
    1
    (* m ((dummy dummy) (- m 1))
 ))))) 5)
)


;;
(display "\n")
(display
(
 ((lambda (dummy)
  ((lambda (x)
   (lambda (m)
    (if (= m 0)
     1
     (* m (x (- m 1))))))
      (lambda (y) ((dummy dummy) y))))
  (lambda (dummy)
   ((lambda (x)
    (lambda (m)
     (if (= m 0)
      1
      (* m (x (- m 1))))))
       (lambda (y) ((dummy dummy) y))))
 ) 5)
)


;;
(define F
 (lambda (x)
  (lambda (m)
   (if (= m 0)
    1
    (* m (x (- m 1)))))))

(define Y
 (lambda (R)
  ((lambda (dummy)
   (R (lambda (y) ((dummy dummy) y))))
   (lambda (dummy)
   (R (lambda (y) ((dummy dummy) y)))
 ))))

(display "\n")
(display ((Y F) 5) )


;;
(define A
  (lambda (x)
    (lambda (n)
      (if (= n 0)
          0
          (+ n (x (- n 1)))))))

(display "\n")
(display ((Y A) 5) )

;;
;MIT/GNU Scheme running under GNU/Linux
;Type `^C' (control-C) followed by `H' to obtain information about interrupts.
;
;Copyright (C) 2011 Massachusetts Institute of Technology
;This is free software; see the source for copying conditions. There is NO
;warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;
;Image saved on Sunday February 7, 2016 at 10:35:34 AM
;  Release 9.1.1 || Microcode 15.3 || Runtime 15.7 || SF 4.41 || LIAR/x86-64 4.118
;  Edwin 3.116
;
;1 ]=> (load "appendix.scm")
;
;;Loading "appendix.scm"...
;120
;120
;120
;120
;120
;120
;120
;120
;120
;15
;;... done
;;Unspecified return value

