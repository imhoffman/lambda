\section{Introduction}

\subsection{Prior Reading}

Read Church's paper.\footnote{Church, Alonzo.\ An Unsolvable Problem of Elementary Number Theory, American Journal of Mathematics, Vol.\ 58 No.\ 2 (April 1936), 345--363. \href{https://www.jstor.org/stable/2371045?seq=1\#metadata_info_tab_contents}{doi:10.2307/2371045}}
% Truthfully, you could be successful without reading the original paper. Church's work is so important that it has matured in the last 90 years. However, to consider those more mature versions to be the subject itself would be to forget from where we came. Don't forget: not even Hari Seldon could discern that Earth was the origin (if not the Foundation) of humanity. The $\L$ calculus may not be the foundation of mathematics, but it is the foundation of this document.}
To be sure: Church's paper is not straightforward.
As a result, this Tutorial doubles as a reader's guide for his first few pages.
We shall refer directly to Church's paper for most of the fundamental definitions, in part because we must describe the justifiable differences between Lisp syntax and Church's syntax.

For parallel reading in introductory $\L$ notation, we recommend the widely used tutorials by Raul Rojas\footnote{A Tutorial Introduction to the Lambda Calculus (\href{https://arxiv.org/abs/1503.09060}{arXiv:1503.09060})}---from which we borrow heavily---and by Barendregt \& Barendsen.\footnote{\href{https://scholar.google.ca/scholar?cluster=10702067055801208241}{Introduction to Lambda Calculus}.}
In particular, the reader may benefit from Rojas' images of the ``plumbing'' of expressions.
The priority in the present document is to build from Church's conceptual structure up to analogous Lisp code.
The exposition shall begin with rewritten expressions from Church's paper in contemporary notation and as Lisp code; from these basics, logic and arithmetic shall be covered.
%Mention $\alpha$- and $\beta$-reduction here...along with a grab of Church's three rules (only number II of which we keep).
%$\eta$-reduction comes below...

The desire to represent the $\L$ calculus in code is spiritually descended from the work of Landin.\footnote{Landin, P.\ J.\ The Mechanical Evaluation of Expressions, The Computer Journal, Vol.\ 6, No.\ 4 (January 1964), 308--320. \href{https://academic.oup.com/comjnl/article/6/4/308/375725}{doi:10.1093/comjnl/6.4.308}; Landin, P.\ J.\ A Correspondence Between ALGOL 60 and Church's Lambda-notation Part I, Communications of the ACM, Vol.\ 8, No.\ 2 (February 1965), 89--101. \href{https://dl.acm.org/citation.cfm?id=363749}{doi:10.1145/363744.363749}; Landin, P.\ J.\ A Correspondence Between ALGOL 60 and Church's Lambda-notation Part II, Communications of the ACM, Vol.\ 8, No.\ 3 (March 1965), 158--167. \href{https://dl.acm.org/citation.cfm?id=363804}{doi:10.1145/363791.363804}}
In a series a papers, Landin investigated mechanical computation through side-by-side comparisons of $\L$ expressions with code in ALGOL~60.
The interested reader is also directed there for pertinent references to the beginnings of Lisp.

As contemporary references for Lisp code, we recommend the following.
Since Scheme is a rather conceptual language, it is perhaps best covered by a textbook.\footnote{\href{https://www.scheme.com/tspl4/}{The Scheme Programming Language}, \href{https://mitpress.mit.edu/sites/default/files/sicp/index.html}{Structure and Interpretation of Computer Programs}, \href{https://mitpress.mit.edu/books/little-schemer-fourth-edition}{Little Schemer}, or \href{https://htdp.org/}{How to Design Programs}}
Common Lisp is comparatively syntactical and is perhaps best covered by coding manuals.\footnote{\href{https://lispcookbook.github.io/cl-cookbook/}{The Common Lisp Cookbook}, \href{http://www.paulgraham.com/onlisp.html}{On Lisp}, or \href{https://www.gnu.org/software/clisp/}{The GNU CLISP manual}}
The audience for our Tutorial is assumed to be comfortable with both university-level mathematics and computer programming; but, it is not assumed that the reader has had any significant exposure to $\L$ calculus and/or Lisp.

\subsection{Code blocks}

In order to provide programming examples as part of the exposition, Scheme and Common Lisp (CL) blocks shall be set side-by-side whenever code is used.
The two dialects are used simultaneously in order to stress syntax-independent concepts.
In all blocks, Scheme appears on the left and Common Lisp on the right.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
{\sf Scheme}\\
\begin{minted}[numbers=none]{racket}
(exit)
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{-2pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
{\sf Common Lisp}\\
\begin{minted}[numbers=none]{lisp}
(quit)
\end{minted}
\end{minipage}
