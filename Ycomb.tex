\section{An Empirical Derivation of Y}

Although there are many equivalent expressions for the $\what{Y}$ combinator, not all formulations that follow immediately from an $\eta$-expansion of the fixpoint relation
\[
f (\what{Y} f) = \what{Y} f = \L{x}.(\what{Y}f)x
\]
can be ``strictly'' evaluated by a hardware implementation.
Indeed, Curry's paradoxical combinator
\[
\what{Y} \equiv \L{f}.\left(\L{x}.f(xx)\right)\left(\L{x}.f(xx)\right)
\]
only completes its code execution under the conditions of ``lazy'' evaluation.
Other common lazy formulations\footnote{see \href{https://en.wikipedia.org/wiki/Fixed-point_combinator\#Other_fixed-point_combinators}{Wikipedia}} include
\[
\what{Y} \equiv \L{f}.(\L{x}.xx)\left(\L{x}.f(xx)\right)
\]
and Turing's
\[
\what{Y} \equiv \left(\L{x}.\L{y}.(y(xxy))\right)\left(\L{x}.\L{y}.(y(xxy))\right)\ .
\]
These formulations are, in a sense, {\em designed} to run forever if strictly evaluated.

What follows is not a symbolic derivation of a lazy formulation; rather, a combinator suitable for strict evaluation shall be found.
The exposition in this document shall end with a combinator of the form
\[
\what{Y} \equiv \L{f}.\Bigg(\L{x}.\bigg(f\Big(\L{y}.\big((xx)y\big)\Big)\bigg)\Bigg)\Bigg(\L{x}.\bigg(f\Big(\L{y}.\big((xx)y\big)\Big)\bigg)\Bigg)\ .
\]
The result shall be obtained through successive strictly-evaluated Lisp steps.
Since each of the steps runs under strict evaluation, it follows that the final result runs under strict evaluation.
In that sense, the result is ``empirical'' and may be understood through interpretation once the result has been obtained.

\subsection{Factorial Example\label{appendix:factorial}}

A classic example for recursion is the mathematical factorial operation.
In this section is devised a method for executing the self-calls that are needed for a recursive factorial function.
The general approach for obtaining and executing the recursions may then be applied to other recursive problems.

Since this derivation is based on operational code, it is worthwhile to gain some perspective from a brief consideration of factorial functions implemented in the Fortran language and the C language.
In the C and Fortran cases below, the entirety of the programs are shown.
That is, both the function definition and its call for an example of $5!$ are shown; in each case the variable $k$ shall be assigned the value 120.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
{\sf C}\\
\begin{minted}[numbers=left]{c}
int factorial(int m) {
 if ( m == 0 ) return 1;
 else return m*factorial(m-1);
}







int main(void) {
 int n, k;

 n = 5;
 k = factorial(n);
 return 0;
}
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
  \vspace{0pt} % anchor for [t]
{\sf Fortran}\\
\begin{minted}[numbers=right,escapeinside=||]{fortran}
integer function factorial(m, dummy)
integer m, dummy
external dummy
if ( m .eq. 0 ) then
  factorial = 1
else
  factorial = m*dummy(m-1,dummy) |\label{fortran:dummy}|
end if
return
end function factorial

program main
integer n, k, factorial
external factorial
n = 5
k = factorial(n,factorial)|\label{fortran:fact}|
stop
end program main
\end{minted}
\end{minipage}

In the C code, the function makes a call to itself---here, ``itself'' means the memory address at which the function's instructions reside---and the call stack grows with additional instances of those instructions.\footnote{This is the ``shape'' that is described in \href{https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book-Z-H-11.html}{Sec~1.2.1 of SICP}.}
In the Fortran code, the factorial function takes two arguments: a function and a value.
The calling function is independent of the called function---they are {\tt external} of each other.
For this recursive example, the calling function and the called function are the same instructions in memory and the call stack nevertheless grows the same in Fortran as it does in C.
(It is worthwhile noting that the focus here is {\em not} on optimizing tail calls or otherwise managing the stack; we merely want to realize a recursive use of the instructions.\footnote{The general treatment is to have the self-call be the tail call: for Scheme, see Sec~3.5 of \href{https://schemers.org/Documents/Standards/R5RS/HTML/}{R5RS} and Sec~5.4.2 of \href{https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book-Z-H-4.html}{SICP}; for Common Lisp, a nice description appears in \href{http://www.cs.sfu.ca/CourseCentral/310/pwfong/Lisp/2/tutorial2.html}{Chapter 2 of this online tutorial}, although it must be noted that CL may behave differently as an interpreter than as a compiler.})

The goal is to write a Lisp program that accomplishes the self-calls.
C has built-in support for the recursive use of a function name being used in its own definition.
Fortran~77 has been used here because it does not have built-in support for recursion, but it can be done; high-level instructions must explicitly be provided that direct the program to the desired procedure in memory.\footnote{In other words, there is apparently nothing at the assembly/opcode level that precludes high-level circular reasoning; Fortran (77) simply does not provide a simple interface.}
Lisp can behave like Fortran or like C; examples of each shall be shown and then a Fortran-like construction shall be pursued.
The goal is to code up the Lisp self-calls as a procedure.

The following examples are two implementations of a Lisp factorial function.
The first version---highlighted in red---is analogous to the C version in which the name of the function is recursively used.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[bgcolor=error]{scheme}
(define (factorial m)
  (if (= m 0)
       1
       (* m (factorial (- m 1)))))

(factorial 5)
; Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[bgcolor=error]{lisp}
(defun factorial (m)
  (if (= m 1)              
      1                           
      (* m (factorial (- m 1))))) 

(factorial 5)
; Value: 120
\end{minted}
\end{minipage}

For the second example, the codes have been rewritten so that the procedure is not circularly defined but is instead explicitly---and therefore innocuously---passed to itself.
That is, the preceding code is more like the C example while the following code is more like the Fortran~77 example.
The following code achieves the same result as the preceding code (i.e., a final, integer value) with the use of an additional $\L$ such that the return from the recursed function is a procedure rather than a value.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{scheme}
(define factorial 
 (lambda (m) 
  (if (= m 0)
   1
   (* m (factorial (- m 1))))))

(factorial 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
(defun factorial () (function
 (lambda (m)
  (if (= m 0)
   1
   (* m (funcall (factorial) (- m 1)))))))

(funcall (factorial) 5)
;Value: 120
\end{minted}
\end{minipage}

Unlike in Scheme, in CL the namespaces are typed such that values are treated differently than functions.\footnote{This difference is described well in \href{https://lispcookbook.github.io/cl-cookbook/functions.html}{The Common Lisp Cookbook}. Or, as Paul Graham states on p.~12 of {\it On Lisp}: ``We can have a variable called {\tt foo} and a function called {\tt foo}, and they need not be identical. This situation can be confusing, and leads to a certain amount of ugliness in code, but it is something that Common Lisp programmers have to live with.''}
Since a procedure is being returned by {\tt factorial}, the return needs to be explicitly defined as a {\tt function} (of zero arguments) and that return needs to be explicitly applied as a function using {\tt funcall}.
That is, {\tt (factorial)} returns an anonymous function which {\tt funcall} applies to the argument {\tt 5}; the last line may be semantically understood as {\tt (\textcolor{red}{(}funcall (factorial)\textcolor{red}{)} 5)} even though CL does not require the parentheses for applying {\tt funcall}.
As mentioned elsewhere in this Tutorial,\footnote{We repeat the quotation from a Scheme user that we included in our Definitions section: `This syntax is often referred to, somewhat pejoratively, as the ``defun'' syntax for {\tt define}, after the {\tt defun} form provided by Lisp languages in which procedures are more closely tied to their names.'} the $\what{\text{hat}}$ notation that has been introduced in this Tutorial in order to keep track of functional behavior is manifest in CL as explicit definitions, calls, and namespaces.\footnote{For yet more perspective, the reader is encouraged consider the implementation of anonymous functions in the Go language, described \href{https://golang.org/ref/spec\#Function\_literals}{at this link}. Go is typed: for example, {\tt fmt.Printf("The return is \%d", func( n int ) int \{ return n + n \} (7) ) } prints ``14''.}

The exposition continues by further embedding $\L$'s in order to facilitate the passing of procedures as arguments, as in the ``Fortran version'' of the Lisp code.
In the next code block, {\tt bzbz} is defined as a procedure that handles the procedure return of the preceding example.\footnote{I name this function ``busy, busy'' as a continuing homage to Vonnegut's {\it Cat's Cradle} in the Y Combinator world---a phenomenon that apparently began with \href{https://mvanier.livejournal.com/2897.html}{Darius Bacon}.}

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
(define bzbz
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
       1
       (* m ((dummy dummy) (- m 1)))|\label{scheme:dummy}|
 ))))

(define factorial
  (lambda (n)
    ((bzbz bzbz) n)|\label{scheme:fact}|
    ))

(factorial 5)
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{lisp}
(defun bzbz () (function
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
    1
    (* m (funcall (funcall dummy dummy) (- m 1)))|\label{cl:dummy}|
 )))))

(defun factorial () (function
 (lambda (n)
  (funcall (funcall (bzbz) (bzbz)) n)|\label{cl:fact}|
 )))

(funcall (factorial) 5)
\end{minted}
\end{minipage}

In the preceding Lisp code, the following parallels may be recognized.
Line~\ref{scheme:dummy} in the Lisp code is analogous to line~\ref{fortran:dummy} in the initial Fortran example.
And, line~\ref{scheme:fact} in the Lisp code is analogous to line~\ref{fortran:fact} in the initial Fortran example.

Even more succinctly, the $\eta$-expansion of
\[
\what{\mathtt{factorial}}\ n \equiv \L{n}.\left(\what{\mathtt{bzbz}}\ \what{\mathtt{bzbz}}\right)n
\]
may be expressed as simply
\[
\what{\mathtt{factorial}} \equiv \left(\what{\mathtt{bzbz}}\ \what{\mathtt{bzbz}}\right)\ ,
\]
which is expressed in Lisp code as

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{scheme}
(define factorial
  (bzbz bzbz))
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
(defun factorial ()
  (funcall (bzbz) (bzbz)))
\end{minted}
\end{minipage}

Next, the code of {\tt bzbz} is swapped in.
There are two different instances of {\tt bzbz} at work and they shall---for clarity of their scopes---be denoted with {\tt 1} and {\tt 2}.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{scheme}
(define factorial (
 (lambda (dummy1)
  (lambda (m1)
   (if (= m1 0)
    1
    (* m1 ((dummy1 dummy1) (- m1 1))))))
 (lambda (dummy2)
  (lambda (m2)
   (if (= m2 0)
    1
    (* m2 ((dummy2 dummy2) (- m2 1))))))))

(factorial 5)
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{lisp}
(defun factorial ()
 ((lambda (dummy1)
   (lambda (m1)
    (if (= m1 0)
     1
     (* m1 (funcall (funcall dummy1 dummy1) (- m1 1))))))
  (lambda (dummy2)
   (lambda (m2)
    (if (= m2 0)
     1
     (* m2 (funcall (funcall dummy2 dummy2) (- m2 1))))))))

(funcall (factorial) 5)|\label{cl:call}|
\end{minted}
\end{minipage}

The next step is an important abstraction away from the machine code---and toward Church.
The program instructions need not be defined with {\tt defun}/{\tt define}, named ``{\tt factorial},'' and called separately on line~\ref{cl:call}; rather, with a single line of code an anonymous function may be applied to $5$ in order to achieve $5!$.
In that case, the code would look like the following blocks.
(The interested reader may wish to express the following code as a single, well-formed Church formula on the numeral $\what{5}$, complete with the logical, multiplication, and predecessor operators!)

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
((
 (lambda (dummy1)
  (lambda (m1)
   (if (= m1 0)
    1
    (* m1 ((dummy1 dummy1) (- m1 1))
 ))))
 (lambda (dummy2)
  (lambda (m2)
   (if (= m2 0)
    1
    (* m2 ((dummy2 dummy2) (- m2 1))
 ))))) 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
(funcall (
 (lambda (dummy1)
  (lambda (m1)
   (if (= m1 0)
    1
    (* m1 (funcall (funcall dummy1 dummy1) (- m1 1))
  ))))
 (lambda (dummy2)
  (lambda (m2)
   (if (= m2 0)
    1
    (* m2 (funcall (funcall dummy2 dummy2) (- m2 1))
  ))))) 5)
;Value: 120
\end{minted}
\end{minipage}

As with all Lisp, the line breaks and indentation mean nothing.
The preceding blocks are one single, long expression.
The scoping in the expression is, of course, handled in terms of Church binding; the output is no different if the code is reverted to the previous, non-indexed names:

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
((
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
    1
    (* m ((dummy dummy) (- m 1))
 ))))
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
    1
    (* m ((dummy dummy) (- m 1))
 ))))) 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
(funcall (
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
    1
    (* m (funcall (funcall dummy dummy) (- m 1))
  ))))
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
    1
    (* m (funcall (funcall dummy dummy) (- m 1))
  ))))) 5)
;Value: 120
\end{minted}
\end{minipage}

It is instructive to examine the ``shape'' of the calls---that is, upon what returns is the program waiting.
The reader is encouraged to examine each and every call and to trace a complete shape.
As a broader examination, the CL program and output below illustrates in which ``busy'' the program is busy.
From the {\tt clisp} prompt:

\begin{minted}{lisp}
[1]> (format t "~& demonstration return: ~d"
 (funcall
 ((lambda (dummy) (format t "~& inside dummy1~%")
   (lambda (m)
    (if (= m 0)
     1
     (* m (funcall (funcall dummy dummy) (- m 1))))))
  (lambda (dummy) (format t "~& inside dummy2~%")
   (lambda (m)
    (if (= m 0)
     1
     (* m (funcall (funcall dummy dummy) (- m 1))))))
 ) 5)
)
 inside dummy1
 inside dummy2
 inside dummy2
 inside dummy2
 inside dummy2
 inside dummy2
 demonstration return: 120
\end{minted}

\noindent
As expected, it is the second block that is spinning.

The final step in getting the code into an interpretable form is to apply $\eta$-expansion to the reference to the {\tt dummy} call so that it may be abstracted out.\footnote{These substitutions represent a seemingly general step. However, for the simplest evaluatable combinator---sometimes called $\what{Z}\equiv\L{f}.\left(\L{x}.f(\L{v}.xxv)\right)\left(\L{x}.f(\L{v}.xxv)\right)$---this is the essential step. See, for example, \href{https://en.wikipedia.org/wiki/Fixed-point_combinator\#Strict_fixed_point_combinator}{the Wikipedia page}.}
In the following code black, two more substitutions are made to the ongoing factorial example: one as {\tt x} and the other as {\tt y}.
Introducing {\tt x} results in moving the call to $\left(\what{\mathtt{dummy}}\,\what{\mathtt{dummy}}\right)$ outside of the first new $\L$ and introducing {\tt y} results in moving the argument {\tt (-~m~1)} outside of the second new $\L$.
The {\tt y} substitution is simply
\[
\L{y}.\left(\what{\mathtt{dummy}}\,\what{\mathtt{dummy}}\right)y \longmapsto \left(\what{\mathtt{dummy}}\,\what{\mathtt{dummy}}\right)
\]
while the {\tt x} substitution is more apparent from the following code.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
(
 ((lambda (dummy)
  ((lambda (x)|\label{R1beg}|
   (lambda (m)
    (if (= m 0)
     1
     (* m (x (- m 1))))))|\label{R1end}|
      (lambda (y) ((dummy dummy) y))))
  (lambda (dummy)
   ((lambda (x)|\label{R2beg}|
    (lambda (m)
     (if (= m 0)
      1
      (* m (x (- m 1))))))|\label{R2end}|
       (lambda (y) ((dummy dummy) y))))
 ) 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
; clisp code
\end{minted}
\end{minipage}

Written this way, it is most apparent that the code from lines \ref{R1beg} to \ref{R1end} (as well as its duplicate code in lines \ref{R2beg} to \ref{R2end}) is specific to the factorial function---namely, details such as multiplication and decrementing the argument $m$.
The code {\em outside} of those blocks serves only the general purpose of calling the factorial-specific functionality.
This represents a separation the specific function's innards from the need to call those innards recursively.
The distinction shall be refined by giving separate names and definitions to those two behaviors: $\what{F}$ for the code that handles the factorial-specific arithmetic; $\what{Y}$ for the code that handles the recursive calling.
(In defining subprograms in the script, the code moves away from a single, Church-like well-formed formula---but closer to having an expression for a general-purpose $\what{Y}$ combinator.)

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
(define F
 (lambda (x)
  (lambda (m)
   (if (= m 0)
    1
    (* m (x (- m 1)))))))

(define Y
 (lambda (R)
  ((lambda (dummy)
   (R (lambda (y) ((dummy dummy) y))))
   (lambda (dummy)
   (R (lambda (y) ((dummy dummy) y)))
 ))))

((Y F) 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
% http://rosettacode.org/wiki/Y_combinator#Common_Lisp
\begin{minted}{lisp}
;clisp code 
\end{minted}
\end{minipage}

The first block is the factorial-specific code, which is named $\what{F}$.
The second block is the code that calls $\what{F}$ recursively: the sought-after $\what{Y}$.
Note that $\what{R}$ has been introduced as the name for the code that needs to be called recursively.
In the following sections are examples of other functions that may be achieved by applying this same general-use $\what{Y}$ to the appropriate kernel of code $\what{R}$.

Of course, it is not essential that {\tt Y} and {\tt F} be named as defined blocks of code---this is done only so that the punchline of {\tt ((Y~F)~5)} is succinct.
In many ways, it is preferable to leave all of the functions as anonymous $\L$'s, in which case one would simply swap in the {\tt Y} and {\tt F} code, resulting in a single long line of anonymous calls applied to {\tt 5}. (Not shown here.)

\subsection{Sumtorial Example}

A simple extension of the preceding reasoning pertains to the ``sumtorial'' function.
That is, if factorial multiplies 5 by 4 by 3, and so on, then a sumtorial function may  be envisioned that adds 5 to 4 to 3, and so on.
Thusly defined, the sumtorial of 5 is 15.
A kernel of code $\what{A}$ is needed: over which to recurse with the same $\what{Y}$ as in Section~\ref{appendix:factorial}.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
(define A
  (lambda (x)
    (lambda (n)
      (if (= n 0)
          0
          (+ n (x (- n 1)))))))

((Y A) 5)
;Value: 15
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
; clisp code
\end{minted}
\end{minipage}

\subsection{Max-of-a-list Example}
% https://eecs.ceas.uc.edu/~franco/C511/html/Scheme/ycomb.html
Coming soon and/or exercise for the reader\ldots

\subsection{Fibonacci Example}
% http://rosettacode.org/wiki/Y_combinator#Common_Lisp
% http://rosettacode.org/wiki/Y_combinator#Scheme
Coming soon and/or exercise for the reader\ldots

\subsection{Little Schemer}

The interested reader is directed to a similar discussion in Little Schemer, which empirically builds to the following strictly-evaluated formulation using strings.
\[
\what{Y} \equiv \L{f}.(\L{x}.xx)\Bigg(\L{x}.\bigg(f\Big(\L{y}.\big((xx)y\big)\Big)\bigg)\Bigg)
\]
which appears in their code as

\begin{minted}{scheme}
(define Y
 (lambda (le) (
  (lambda (f) (f f))
  (lambda (f) (
   le (lambda (x) ((f f) x))
 )))))
\end{minted}
