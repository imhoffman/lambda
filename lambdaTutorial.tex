\documentclass[fleqn,12pt,letterpaper]{article}
%\documentclass[fleqn,12pt,letterpaper,twoside]{article}
%\documentclass[fleqn,leqno,12pt,letterpaper]{article}

\usepackage[margin=1.0in, marginparwidth=1.25in, marginparsep=1mm, heightrounded]{geometry}
\setlength{\parskip}{0.15in}
%\setlength{\topmargin}{-0.25in}
%%\setlength{\headheight}{20pt}
%%\setlength{\headsep}{0in}
%\setlength{\textheight}{8.50in}
%\setlength{\textwidth}{6.50in}
%\setlength{\oddsidemargin}{-0.01in}
%\setlength{\evensidemargin}{-0.01in}
%%\setlength{\footskip}{0in}
%%\setlength{\parindent}{0.25in}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{bigints}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{minted}
\usepackage{xcolor}
\usepackage{kantlipsum}
\usepackage{marginnote}
\reversemarginpar



\usepackage{scalerel,stackengine}
\stackMath
\newcommand\what[1]{%
\savestack{\tmpbox}{\stretchto{%
  \scaleto{%
    \scalerel*[\widthof{\ensuremath{#1}}]{\kern-.6pt\bigwedge\kern-.6pt}%
    {\rule[-\textheight/2]{1ex}{\textheight}}%WIDTH-LIMITED BIG WEDGE
  }{\textheight}% 
}{0.5ex}}%
\stackon[1pt]{#1}{\tmpbox}%
}

\usepackage[yyyymmdd]{datetime}
\renewcommand{\dateseparator}{--}
\usepackage{fancyhdr,lastpage}
\pagestyle{fancy}
%\pagestyle{empty}
\lhead{\sc $\lambda$ Tutorial}
\chead{\sc \thepage~of \pageref{LastPage}}
\rhead{I.\ M.\ Hoffman}
\lfoot{}
\cfoot{}
\rfoot{}

\renewcommand{\L}{\lambda}
\definecolor{bg}{rgb}{0.97, 0.97, 0.97}
\definecolor{error}{rgb}{0.95, 0.75, 0.75}
\setminted[racket]{mathescape, breaklines, bgcolor=bg, numbers=left, numbersep=5pt, gobble=0, framesep=2mm}
\setminted[scheme]{mathescape, breaklines, bgcolor=bg, numbers=left, numbersep=5pt, gobble=0, framesep=2mm}
%\setminted[scheme]{mathescape, bgcolor=bg, linenos, numbersep=5pt, gobble=0, frame=lines, framesep=2mm}
\setminted[lisp]{mathescape, breaklines, bgcolor=bg, numbers=right, numbersep=5pt, gobble=0, framesep=2mm}
%\setminted[lisp]{mathescape, bgcolor=bg, numbersep=5pt, gobble=0, frame=lines, framesep=2mm}
\setminted[c]{mathescape, breaklines, bgcolor=bg, numbers=right, numbersep=5pt, gobble=0, framesep=2mm}
\setminted[fortran]{mathescape, breaklines, bgcolor=bg, numbers=left, numbersep=5pt, gobble=0, framesep=2mm}
\setmintedinline{bgcolor={}}
% lang options: scheme, lisp, newlisp, racket


\title{Introduction to Lambda Calculus with Examples in Lisp}
\author{Ian M.\ Hoffman, Nora Aydinyan\\{\small \sc Quest University Canada}}
\date{v0.1, \today}
 
\begin{document}

\maketitle

\begin{abstract}
This is yet another introduction to Alonzo Church's marvellous notation system for representing mathematical operations.
With this document, we hope to show that an understanding of the $\L$ syntax provides a desirable stepping-off point for practical programs.
By providing coding examples in both Common Lisp and Scheme---two major dialects of Lisp---we hope to elucidate the conceptual underpinnings of the approach.
The audience for this text is assumed to have comfort with both university-level mathematics and computer programming, but little exposure to $\L$ calculus and/or Lisp.
\end{abstract}
 
\section{Introduction}
%HelloIAN
\subsection{Prior Reading}

Obviously, read Church's paper.\footnote{Church, Alonzo. An Unsolvable Problem of Elementary Number Theory, American Journal of Mathematics, Vol. 58 No. 2 (April 1936), 345--363.}
Truthfully, you could go without the original paper precisely because Church's work is so important that it has developed some maturity in the last 90 years.
However, to consider those more mature versions to be the subject itself would be to forget from where we came.
Not even Hari Seldon\footnote{I. Asimov, “Foundation”, ISBN 0-553-29335-4, Gnome Press (1951)} could discern that Earth was the origin (if not the Foundation) of humanity; whether or not the $\L$ calculus is the foundation of mathematics, it is the foundation of this document---though we do not treat the obviously imperfect Church paper as sacred scripture.

For prior/parallel reading in introductory $\L$ notation, we recommend the widely used tutorials by Raul Rojas\footnote{A Tutorial Introduction to the Lambda Calculus (\href{https://arxiv.org/abs/1503.09060}{arXiv:1503.09060})}, from which we borrow heavily, and by Barendregt \& Barendsen.\footnote{\href{https://scholar.google.ca/scholar?cluster=10702067055801208241}{Introduction to Lambda Calculus}.}
In particular, we do not describe much of the basic notation that is described by Rojas through the use of images of the ``plumbing.''
Mention $\alpha$- and $\beta$-reduction here...along with a grab of Church's three rules (only number II of which we keep).
$\eta$-reduction comes below...

As references for Lisp code, Scheme is rather conceptual and perhaps best covered by a textbook,\footnote{\href{https://mitpress.mit.edu/sites/default/files/sicp/index.html}{SICP}, \href{https://mitpress.mit.edu/books/little-schemer-fourth-edition}{Little Schemer}} while Common Lisp is comparatively syntactical and perhaps best covered by coding manuals.\footnote{\href{https://www.tutorialspoint.com/lisp/}{Tutorial}, \href{http://cl-cookbook.sourceforge.net/index.html}{Cookbook}, \href{https://www.gnu.org/software/clisp/}{GNU CLISP}}
The audience for this text is assumed to have comfort with both university-level mathematics and computer programming, but little exposure to $\L$ calculus and/or Lisp.

\subsection{Code blocks}

In order to provide programming examples as part of the exposition, Scheme and Common Lisp (CL) blocks shall be set side-by-side whenever code is used.
We use two dialects simultaneously in order to stress syntax-independent concepts.
In all blocks, Scheme appears on the left.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
{\sf Scheme}\\
\begin{minted}[numbers=none]{racket}
(exit)
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{-2pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
{\sf Common Lisp}\\
\begin{minted}[numbers=none]{lisp}
(quit)
\end{minted}
\end{minipage}

\section{Definitions}

\subsection{Everyday Mathematical Notation}

We begin our discussion with examples of what Smith\footnote{Smith, Peter.  An Introduction to Godel’s Theorems, 2nd ed.  Cambridge: Cambridge University Press, 2013.} calls ``everyday'' mathematical statements/expressions re-expressed in $\lambda$ notation, Scheme, and CL.
That is, notation such as
\[
f(x) = \sin(3x+\pi)
\]
is assumed to be familiar.
(However, don't expect to see functions like sine nor irrational numbers like $\pi$.
Very little of the literature on $\L$ investigations---and none of this document---addresses complicated composite functions such as sine, nor real numerical arguments other than the positive integers.)

\begin{itemize}

\item[a.] Everyday statement $f(x)$\ .

In $\L$ notation, this is simply
\[
fx
\]
without any parentheses or $\L$'s.
This expression is merely a statement that $x$ can be fed to $f$.
The instruction $f$ is not defined by this expression; it would be defined elsewhere.

... briefly introduce $\eta$ expansion...but don't get bogged down with binding...yet.
% https://math.stackexchange.com/questions/836075/understanding-%ce%b7-conversion-lambda-calculus

With $x$ bound via $\eta$-expansion, $f$ as a function of $x$ is represented as
\[
\L{x}.fx
\]
in $\L$ notation.
The character following the $\L$ is the independent variable whose value can be assigned from without.
Although it is not uncommon to encounter the following notation---including in Church's original paper---
\renewcommand*{\marginfont}{\color{red}\sffamily}
\[
\L{x}.f(x)\marginnote{confusing}
\]
for function application, it is inappropriate to use parentheses in this manner in Lisp, and so we avoid it.
In this document, we reserve parentheses for grouping, as shall be seen.

The power of $\L$ notation is that simply $fx$ represents the application of the instructions $f$ to $x$.
Read left-to-right in this way, $f$ is obviously an operator that is operating on $x$---such ordering will {\em always} unambiguously communicate this meaning.
Nevertheless, if we need to really emphasize that $f$ is a function (and is taking $x$ as its argument), we shall denote $f$ as the operator $\what{f}$ as in
\renewcommand*{\marginfont}{\color{blue}\sffamily}
\[
\L{x}.\what{f}x\ .\marginnote{operator}
\]
An appropriate use of parentheses is
\[
\L{x}.(fx)\ ,
\]
which is equivalent to $\L{x}.fx$.
In Church notation, the parentheses in the preceding case are optional because $fx$ communicates all that needs to be known.
However, in a programming language, $fx$ would be seen as a single symbol/name, not as $f$ and $x$ in an operator--argument relationship.

Using parentheses to surround the application of $f$ to $x$ with white space between the $f$ and $x$ is how $\L$-based Lisp languages actualize the idea.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{racket}
(f x)
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{lisp}
(f x)
\end{minted}
\end{minipage}

The only special characters in Church's notation are $\L$, {\tt .}, {\tt (}, and {\tt )}.
So, all of the following have the same meaning:
\[
\L{x}.fx\qquad\text{and}\qquad\L{v}.fv\qquad\text{and}\qquad\L{t}.(ft)\qquad\text{and}\qquad\L{x}.\left(\what{f}x\right)
\]
because the $\L$ and the .\ are what matters to the $f$.

\item[b.] Everyday statement $f(17)$\ .

Assuming that we knew the functional form of $f$, we could determine the value of the function when the argument is 17.

The notation is no different for 17 than for $x$.
In Church notation, one symbol up against another means that the symbol on the left operates on the symbol on the right.
So, just as
\[
fx
\]
means $f$ of $x$, so too does
\[
f17
\]
mean $f$ of 17.
Of course, this is wildly unclear because we humans usually use two symbols to represent the value ``seventeen,'' but the $\L$ calculus is a symbolic analysis tool not intended for hauling literals around.
In that sense, it is best not to write ``17'' at all, because you cannot help but convey ``1 acting on 7'' no matter how you use the parentheses.
If ``seventeen'' were instead the single symbol $\mho$ then we would write
\[
\left(\L{x}.fx\right)\mho = f\mho\ .
\]

Of course, we do not have as many symbols as values that we would like to compute.
For that reason, both Scheme and CL allow strings of characters (like ``lambda'') to be single symbols in their own right.
This means that blank space is part of both Scheme and CL and so those languages necessarily depart from Church notation.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{racket}
(f 17)
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{lisp}
(f 17)
\end{minted}
\end{minipage}

In both languages $f$ is not pressed up against ``17'' any more than it was pressed up against $x$. %hello
In both cases, blank space appears between $f$ and its argument.
The outer parentheses that surround $f$ and its argument are necessary for both Scheme and CL---even though they are optional for Church.

Later, we shall describe that numerals in $\L$ calculus are functions, not literals.
With this recognition, we can obviate the ambiguity of ``17'' with our operator notation: $\what{17}$ means ``seventeen'' while $\what{1}\what{7}$ means ``1 acting on 7.''

Please pause now to read the $\mho$ equation in this section in terms of Rojas' plumbing diagrams, if you haven't already.
You should consider the diagrams again when you read Equation~\ref{fgx} in the next section.

\item[c.] Everyday statement $f\left(g(x)\right)$\ .

We wish to represent passing an argument to $f$ that is the value of the function $g$ when given the argument $x$.
In $\L$ notation, this is simply
\[
\L{x}.fgx\ .
\]
We could arrive at this from a separate $f$ and $g$ as follows:
\begin{align*}
\text{given~~} & \L{v}.fv \\
\text{given~~} & \L{u}.gu \\
\text{then~~}  & \left(\L{v}.fv\right)\left(\L{u}.gu\right)x \\ \tag{1.1}\label{fgx}
             & \left(\L{v}.fv\right)\left(gx\right) \\
             & \left(\L{v}.fv\right)\left(gx\right) \\
             & fgx\ .
\end{align*}
The right-to-left application is essential; the steps occur in this order: (1) the value $x$ is fed to the function $g$, (2) the function $g$ returns a value that depends on $x$, (3) the value returned from $g$ gets fed to the function $f$, and finally (4) the function $f$ returns a value that depends on the value returned from $g$.
In order to remember this order, one may correctly write
\[
\L{x}.f(gx)
\]
but adding the following additional parentheses
\renewcommand*{\marginfont}{\color{red}\sffamily}
\[
\L{x}.f\left(g(x)\right)\marginnote{confusing}
\]
would be mixing the meaning of the parentheses between their Church meaning and their ``everyday'' meaning.
As before, Lisp expects a proper, consistent use of parentheses.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{racket}
(f (g x))
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{lisp}
(f (g x))
\end{minted}
\end{minipage}

\item[d.] Everyday statement $f(x) = 3x$\ .

Stating that $f$ comprises a particular set of instructions requires an assignment of those instructions to $f$.

NB: Our $\lambda$ notation in this introductory section stops short of a complete definition of the operations in the example functions.
The development of a primitive notion/notation of, say, ``multiplication'' is the end goal of the $\lambda$ calculus (and of this document).
In this section, we describe all of the $\L$ semantics, but not all of the $\L$ syntax.

\item[e.] Composing functions.

\renewcommand*{\marginfont}{\color{blue}\sffamily}
In contrast to ``$f$ of $g$ of $x$,'' consider the following: ``{\em first} apply the function $f$ to the function $g$ in order to make some new function {\em and then} feed the value $x$ to that new function.''
This is a wildly important idea.
We can already tell that its $\L$ notation is
\[
\L{x}.(fg)x\ .
\]
Furthermore, note that we did {\em not} need to give the new function $(fg)$ a name like $h$.
The $\L$ expression is tantamount to a name, which is why the term ``anonymous\marginnote{anonymous} function'' is often used in its programming.

In the preceding examples, $x$ could generally be anything: a literal value, a returned value from a different function, or a function itself.
If $x$ is itself a function, then $\L{x}.fx$ does not represent a value but rather a function.
That is, although Church's notation is not {\em typed}\marginnote{typed}, most programming languages are---it must be known whether a value is being returned versus, say, the memory address of a set of functional instructions.
Scheme and CL differ in how flexibly the user may pass an arbitrary type.
In code, Scheme behaves like $\L$ (in that $f$ and $x$ could be anything) while CL requires the identification of functions versus primitive values.\footnote{A nice description of the difference can be found \href{http://cl-cookbook.sourceforge.net/functions.html}{here}.}



\end{itemize}

\subsection{Free and bound variables}

Having a free variable means that information must be added in order for the expression to be evaluated.
Equivalently, if all of the variables are bound, then the expression is ready to run.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{racket}
> (lambda (x) (* x 3))
#<procedure>
> ((lambda (x) (* x 3)) 7)
21
> (lambda (u) (* u v))
#<procedure>
> ((lambda (u) (* u v)) 7)
v: undefined;
> ((lambda (u) (* u v)) 7 3)
#<procedure>: arity mismatch;
 the expected number of arguments does not match the given number
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em}
  \vspace{0pt}
  \vspace{\dimexpr\ht\strutbox-\topskip}
\begin{minted}[numbers=none]{lisp}
[1]> (lambda (x) (* x 3))
#<FUNCTION :LAMBDA (X) (* X 3)>
[2]> ((lambda (x) (* x 3)) 7)
21
[3]> (lambda (u) (* u v))
#<FUNCTION :LAMBDA (U) (* U V)>
[4]> ((lambda (u) (* u v)) 7)
*** - :LAMBDA: variable V has no value
[6]> ((lambda (u) (* u v)) 7 3)
*** - EVAL/APPLY: too many arguments given to :LAMBDA
\end{minted}
\end{minipage}

\noindent
At the interpreter prompt, a $\L$ expression may be entered and recognized as yet-unapplied function instructions, whether the variables are bound or not.
As shown, the first expression has $x$ which is bound and the second expression has $u$ which is bound and $v$ which is not bound.
In order to evaluate the expression, the function must be applied to a value.
Since $v$ is not bound in the second expression, there is no way to pass a value for $v$ into the function instructions.

\section{Mark-up Testing}

Some text leading up to the code block.


\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{scheme}
((lambda (x) (* x x)) 5)
(Y F)
((lambda (x) (Y x)) F)
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
%  \vspace{-2pt} % anchor for [t]
\begin{minted}{lisp}
((lambda (x) (* x x)) 5)
(Y F)
((lambda (x) (Y x)) F)
((lambda (x) (* x x)) 5)
(Y F)
((lambda (x) (Y x)) F)
\end{minted}
\end{minipage}

And some more text after the end of the code block.

\section{Conditionals}

In this section of the tutorial, Rojas introduces the main conditional operators in Lambda Calculus.\footnote{A Tutorial Introduction to the Lambda Calculus (\href{https://arxiv.org/abs/1503.09060}{arXiv:1503.09060})}. These operators are "true" and "false". In both cases, they take two arguments. However, "true" returns the first one, and "false" returns the second one. We can clearly see it in their formulas.
The formula for "true" takes arguments x and y, while returning x.
\[
\what{T} \equiv \L{xy.x}
\]

"False, on the other hand, will return y, or the second argument in the input.
\[
\what{F} \equiv \L{xy.y}
\]

\subsection{Logical Operations}
This section focuses on logical operations using the knowledge of conditional operators "true" and "false" that we Discussed above.

\begin{itemize}
\item[a.] AND
\[
AND \equiv \L{xy.xyF}
\]

To explain what this operation does exactly, we can look at the particular example of inputting arguments "true" and "false" in this formula.
\[
AND \equiv \L{xy.xyF}(\what{T})(\what{F})
\]
Then, using beta - reduction, we can input "true" inside the formula as the first argument x, and "false" as the second argument y.
These steps give us the following.
\[
\L{xy.xyF}(\what{T})(\what{F}) = \L{y}.TyF(\what{F}) = TFF
\]
If we unfold the formula of the first true, we can see that the first operator (in this case, "true") will choose between the following operators (in this case the first one). Thus, the answer will be false.
\[
\L{xy}.x(F)(F) = F
\]
Similarly, we can use other different inputs.
If the first argument is "false" and the second argument is true, we get the following formula
\[
\L{xy.xyF}(\what{F})(\what{T}) =  FTF
\]
leading us to derive the answer.
\[
\L{xy}.y(T)(F) = F.
\]
Thus, we can see that the AND operation will only output "true" if both arguments are true. 
INSERT A TABLE HERE MAYBE?

\item[b.] OR

This function works similarly as AND, except that it will output 'true" if at least one of the arguments is true and it will output false only if both arguments are "false".

\[
OR \equiv \L{xy}.x\what{T}y
\]

\item[c.] NOT

\[
NOT \equiv \L{x}.x \what{F} \what{T}
\]


\end{itemize}



\appendix

\section{An Empirical Derivation of Y}

Although there are many equivalent expressions for the $\what{Y}$ combinator, not all formulations that follow immediately from an $\eta$-expansion of the fixpoint relation
\[
f (\what{Y} f) = \what{Y} f = \L{x}.(\what{Y}f)x
\]
can be ``strictly'' evaluated by a hardware implementation.
Indeed, Curry's paradoxical combinator
\[
\what{Y} \equiv \L{f}.\left(\L{x}.f(xx)\right)\left(\L{x}.f(xx)\right)
\]
only completes its code execution under the conditions of ``lazy'' evaluation.
Other common lazy formulations\footnote{see \href{https://en.wikipedia.org/wiki/Fixed-point_combinator\#Other_fixed-point_combinators}{Wikipedia}} include
\[
\what{Y} \equiv \L{f}.(\L{x}.xx)\left(\L{x}.f(xx)\right)
\]
and Turing's
\[
\what{Y} \equiv \left(\L{x}.\L{y}.(y(xxy))\right)\left(\L{x}.\L{y}.(y(xxy))\right)\ .
\]
These formulations are, in a sense, designed to run forever if strictly evaluated.\footnote{There may be ways around this, as long as the self-call in the code being recursed is the tail call: see Sec~3.5 of \href{https://schemers.org/Documents/Standards/R5RS/HTML/}{R5RS} and Sec~5.4.2 of \href{https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book-Z-H-4.html}{SICP}; Common Lisp may behave differently as an interpreter than as a compiler.}

In what follows, we shall not symbolically derive a lazy formulation but shall instead arrive at a strict combinator.
Ours shall have the form
\[
\what{Y} \equiv \L{f}.\Bigg(\L{x}.\bigg(f\Big(\L{y}.\big((xx)y\big)\Big)\bigg)\Bigg)\Bigg(\L{x}.\bigg(f\Big(\L{y}.\big((xx)y\big)\Big)\bigg)\Bigg)\ .
\]
We shall arrive at our result through successive strictly-evaluated Lisp steps.
Since each of our steps runs under strict evaluation, we are guaranteed that our final result runs under strict evaluation.
In that sense, the result is ``empirical'' and we may seek to understand it by interpreting the result after we have obtained it.

\subsection{Factorial Example\label{appendix:factorial}}

A classic example for recursion is the factorial operation.
We shall devise a method for executing the self-calls in a factorial function, and then apply that method portably to other recursions.

Since this derivation is based on operational code, we begin by gaining some perspective from a brief consideration of the factorial function implemented in the fortran language and the C language.
In the C and fortran cases below, the entirety of the programs are shown.
That is, both the function definition and its call for an example of $5!$ are shown; in each case the variable $k$ shall be assigned the value 120.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
{\sf C}\\
\begin{minted}[numbers=left]{c}
int factorial(int n) {
 if (n==0) return 1;
 else return n*factorial(n-1);
}







int main(void) {
 int n, k;
 n = 5;
 k = factorial(n);
 return 0;
}
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
  \vspace{0pt} % anchor for [t]
{\sf Fortran}\\
\begin{minted}[numbers=right,escapeinside=||]{fortran}
integer function factorial(m, dummy)
integer m, dummy
external dummy
if ( m .eq. 0 ) then
  factorial = 1
else
  factorial = m*dummy(m-1,dummy) |\label{fortran:dummy}|
end if
return
end function factorial

program main
integer n, k, factorial
external factorial
n = 5
k = factorial(n,factorial)|\label{fortran:fact}|
stop
end program main
\end{minted}
\end{minipage}

In the C code, the function makes a call to itself---here, ``itself'' means the memory address at which the function's instructions reside---and the call stack grows with additional instances of those instructions.\footnote{This is the ``shape'' that is described in \href{https://mitpress.mit.edu/sites/default/files/sicp/full-text/book/book-Z-H-11.html}{Sec~1.2.1 of SICP}.}
In the fortran code, it is more apparent that the factorial function takes two arguments: a function and a value.
The calling function is independent of the called function---they are {\tt external} of each other.
Although they are nevertheless the same instructions in memory, each call to those instructions is an individual call on the stack.

Our goal is to write a Lisp program that accomplishes the self-calls.
As we have seen, C has built-in support for the recursive use of a function name being used in its own definition.\footnote{It is possible but not necessarily wise for a pre-processor, compiler, interpreter, etc., to be programmed to handle explicit circular reasoning.}
Fortran does not have built-in support, which simply means that we must explicitly instruct the program to look to our desired instructions in memory.
Lisp is more like fortran than C, in this case.
Indeed, we shall need to identify the Lisp self-call as a procedure.
Below are two versions of a Lisp definition: both of which are syntactically accepted by the interpreter, but only the second of which can be run.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[bgcolor=error]{scheme}
(define (factorial n)
  ((if (= n 0)
       1
       (* n (factorial (- n 1))))))

(factorial 5)
;The object 1 is not applicable.
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[bgcolor=error]{lisp}

\end{minted}
\end{minipage}

The preceding code does not work, but returning {\tt factorial} as a $\L$-defined procedure {\em does} work.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{scheme}
(define factorial 
  (lambda (n) 
    (if (= n 0)
        1
        (* n (factorial (- n 1))))))

(factorial 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}

\end{minted}
\end{minipage}

Thus, the Lisp version of the fortran code is as follows.
Note that {\tt bzbz} is {\em not} a special term in the interpreter; it could be called anything.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
(define bzbz
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
       1
       (* m ((dummy dummy) (- m 1)))|\label{scheme:dummy}|
 ))))

(define factorial
  (lambda (n)
    ((bzbz bzbz) n)|\label{scheme:fact}|
    ))

(factorial 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
; clisp code
\end{minted}
\end{minipage}

In the preceding Scheme code, the following parallels may be recognized.
Line~\ref{scheme:dummy} in the Scheme code is analogous to line~\ref{fortran:dummy} in our initial fortran example.
And, line~\ref{scheme:fact} in the Scheme code is analogous to line~\ref{fortran:fact} in our initial fortran example.

Even more succinctly, we can express the $\eta$-expansion of
\[
\what{\mathtt{factorial}}\ n \equiv \L{n}.\left(\what{\mathtt{bzbz}}\ \what{\mathtt{bzbz}}\right)n
\]
as simply
\[
\what{\mathtt{factorial}} \equiv \left(\what{\mathtt{bzbz}}\ \what{\mathtt{bzbz}}\right)\ ,
\]
which is expressed in Lisp code as

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
(define factorial
  (bzbz bzbz)
 )
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
; clisp code
\end{minted}
\end{minipage}

Now, we swap in the code of {\tt bzbz}.
Since there are two different calls to {\tt bzbz}, we shall denote---for now; we'll revert later---the two different instances of {\tt bzbz} with {\tt 1} and {\tt 2} in order to make their scopes explicit.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
(define factorial (
 (lambda (dummy1)
  (lambda (m1)
   (if (= m1 0)
    1
    (* m1 ((dummy1 dummy1) (- m1 1))
 ))))
 (lambda (dummy2)
  (lambda (m2)
   (if (= m2 0)
    1
    (* m2 ((dummy2 dummy2) (- m2 1))
 ))))))

(factorial 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
; clisp code
\end{minted}
\end{minipage}

As one additional step of abstraction, we may remove the naming of these instructions as {\tt factorial} and simply apply an anonymous function to $5$ in order to achieve $5!$.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
((
 (lambda (dummy1)
  (lambda (m1)
   (if (= m1 0)
    1
    (* m1 ((dummy1 dummy1) (- m1 1))
 ))))
 (lambda (dummy2)
  (lambda (m2)
   (if (= m2 0)
    1
    (* m2 ((dummy2 dummy2) (- m2 1))
 ))))) 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
; clisp code
\end{minted}
\end{minipage}

\noindent which, of course, is no different if we revert to our previous recycled function names:

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
((
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
    1
    (* m ((dummy dummy) (- m 1))
 ))))
 (lambda (dummy)
  (lambda (m)
   (if (= m 0)
    1
    (* m ((dummy dummy) (- m 1))
 ))))) 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
; clisp code
\end{minted}
\end{minipage}

The final step in getting our code into an interpretable form is to apply $\eta$-expansion to the reference to the {\tt dummy} call so that it may be abstracted out.\footnote{With these substitutions, we make a general step. For the simplest evaluatable combinator---sometimes called $\what{Z}\equiv\L{f}.\left(\L{x}.f(\L{v}.xxv)\right)\left(\L{x}.f(\L{v}.xxv)\right)$---this is the essential step. See, for example, \href{https://en.wikipedia.org/wiki/Fixed-point_combinator\#Strict_fixed_point_combinator}{the Wikipedia page}.}
In the following code, we make two substitutions: one as {\tt x} and the other as {\tt y}.
Introducing {\tt x} results in moving the call to $\left(\what{\mathtt{dummy}}\,\what{\mathtt{dummy}}\right)$ outside of the first new $\L$ and introducing {\tt y} results in moving the argument {\tt (-~m~1)} outside of the second new $\L$.
The {\tt y} substitution is simply
\[
\L{y}.\left(\what{\mathtt{dummy}}\,\what{\mathtt{dummy}}\right)y \longmapsto \left(\what{\mathtt{dummy}}\,\what{\mathtt{dummy}}\right)
\]
while the {\tt x} substitution is more apparent from the following code.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
(((lambda (dummy)
 ((lambda (x)|\label{R1beg}|
  (lambda (m)
   (if (= m 0)
    1
    (* m (x (- m 1))))))|\label{R1end}|
     (lambda (y) ((dummy dummy) y))
 ))
 (lambda (dummy)
  ((lambda (x)|\label{R2beg}|
   (lambda (m)
    (if (= m 0)
     1
     (* m (x (- m 1))))))|\label{R2end}|
      (lambda (y) ((dummy dummy) y))
 ))
 ) 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
; clisp code
\end{minted}
\end{minipage}

Written this way, it is most apparent that the code from lines \ref{R1beg} to \ref{R1end} (as well as its duplicate code in lines \ref{R2beg} to \ref{R2end}) is specific to the factorial function---namely, details such as multiplication and decrementing the argument $m$.
The code outside of those blocks serves only to call the factorial functionality.
This is our first success in separating the specific function's innards from the need to call those innards recursively.
Let us continue to refine that distinction by giving names to those two behaviors.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
(define F
  (lambda (x)
    (lambda (m)
      (if (= m 0)
          1
          (* m (x (- m 1)))))))

(define Y
 (lambda (R)
  ((lambda (dummy)
   (R (lambda (y) ((dummy dummy)y)))
   )(lambda (dummy)
   (R (lambda (y) ((dummy dummy)y)))
 ))))

((Y F) 5)
;Value: 120
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
% http://rosettacode.org/wiki/Y_combinator#Common_Lisp
\begin{minted}{lisp}
;clisp code 
(defun F () (
(lambda (x) (lambda (m)
(if (= m 0) 1 (* m (funcall x (- m 1))))))))

; example call
(defun G () (function (lambda (n) (+ n 3))))
(funcall (G) 3)

(defun Y () (lambda(f) (lambda(x) (funcall f (lambda(y) ((funcall x x) y))))(lambda(x) (funcall f (lambda(y) ((funcall x x) y))))))

*** - SYSTEM::%EXPAND-FORM: (FUNCALL X X) should be a lambda expression
\end{minted}
\end{minipage}

The first block is our factorial-specific code, which we name $\what{F}$.
The second block is the code that calls $\what{F}$ recursively: our sought-after $\what{Y}$.
Note that we introduce $\what{R}$ for whatever code needs to be called recursively.
In the following sections are examples of other functions that may be achieved by applying this same general-use $\what{Y}$ to the appropriate kernel of code $\what{R}$.

Of course, it is not essential that we name {\tt Y} and {\tt F} as defined blocks of code---we only did so because the punchline of {\tt ((Y~F)~5)} is succinct.
In many ways, it is preferrable to leave all of the functions as anonymous $\L$'s, in which case one would simply swap in the {\tt Y} and {\tt F} code, resulting in a single long line of anonymous calls applied to {\tt 5}. (Not shown here.)

\subsection{Sumtorial Example}

A simple extension of the preceding reasoning pertains to the ``sumtorial'' function.
That is, if factorial multiplies 5 by 4 by 3, and so on, then we can envision a sumtorial function that adds 5 to 4 to 3, and so on.
Thusly defined, the sumtorial of 5 is 15.
We need only devise a kernel of code $\what{A}$ over which to recurse with the same $\what{Y}$ as in Section~\ref{appendix:factorial}.

\noindent
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}[escapeinside=||]{scheme}
(define A
  (lambda (x)
    (lambda (n)
      (if (= n 0)
          0
          (+ n (x (- n 1)))))))

((Y A) 5)
;Value: 15
\end{minted}
\end{minipage}
\begin{minipage}[t]{0.5\linewidth-0.5em} % some separation
  \vspace{0pt} % anchor for [t]
  \vspace{\dimexpr\ht\strutbox-\topskip}% remove excess glue
\begin{minted}{lisp}
; clisp code
\end{minted}
\end{minipage}

\subsection{Max-of-a-list Example}
% https://eecs.ceas.uc.edu/~franco/C511/html/Scheme/ycomb.html

\subsection{Fibonacci Example}
% http://rosettacode.org/wiki/Y_combinator#Common_Lisp
% http://rosettacode.org/wiki/Y_combinator#Scheme

\subsection{Little Schemer}

The interested reader is directed to a similar discussion in Little Schemer, which empirically builds to the following strictly-evaluated formulation using strings.
\[
\what{Y} \equiv \L{f}.(\L{x}.xx)\Bigg(\L{x}.\bigg(f\Big(\L{y}.\big((xx)y\big)\Big)\bigg)\Bigg)
\]
which appears in their code as

\begin{minted}{scheme}
(define Y
 (lambda (le) (
  (lambda (f) (f f))
  (lambda (f) (
   le (lambda (x) ((f f) x))
 )))))
\end{minted}


\end{document}
