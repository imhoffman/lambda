A Code-based Tutorial for the Lambda Calculus
=============================================

As a professor, I went back to school and enrolled as a student in a course on Church's 1936 paper.
My end-of-class project was intended to be an entire Tutorial, but ended up being only [the appendix that is linked here](Appendix.pdf) on the Y combinator.
(The C and Fortran code in the examples is borrowed from [a lesson in my course on numerical methods](IND3156.pdf).)
The goal of the Tutorial is to have Scheme and Common Lisp code accompany the exposition&mdash;[here](appendix.scm) and [here](appendix.lisp) are links to the code used in the appendix.

**Note** that downloading the PDFs rather than viewing them on gitlab will provide access to the many live hyperlinks in the documents.

The entire Tutorial has proven to be a much larger project that now includes a research student of mine.
Updates will appear here (including to the appendices) as we make progress.

